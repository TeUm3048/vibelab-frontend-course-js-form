"use strict";

const personalMovieDB = {
  count: 0,
  movies: {},
  actors: {},
  genres: [],
  privat: false,

  /* The `showMyDB` function is a method of the `personalMovieDB` object. It is used to check if the
  `privat` property of the `personalMovieDB` object is set to `false`. If it is `false`, the
  function returns the `personalMovieDB` object itself. If it is `true`, the function returns
  `null`. This function is used to determine whether the database should be displayed or not. */
  showMyDB: function () {
    if (!this?.privat) {
      return this;
    }
    return null;
  },

  /* The `writeYourGenres` method is a function that prompts the user to enter their favorite movie
  genres. It takes an optional parameter `genreCount` which defaults to 3. */
  writeYourGenres: function (genreCount = 3) {
    for (let i = 1; i <= genreCount; i++) {
      let genre = prompt(`Ваш любимый жанр под номером ${i}`);

      /* The `while` loop is used to validate the user input for the genre name. */
      while (!genre?.length > 0) {
        genre = prompt(`Ваш любимый жанр под номером ${i}`);
      }

      this.genres.push(genre);
    }

    this.genres.forEach((genre, ind) => {
      console.log(`Любимый жанр #${ind + 1} - это ${genre}`);
    });
  },

  /**
   * The method `validateMovie` checks if a movie is valid based on its length and if it already exists
   * in a database.
   * @param {string | null} movie - The `movie` parameter is a string that represents the name of a movie.
   * @returns {boolean} The function `validateMovie` returns `true` if the `movie` meets all the validation
   * criteria, and `false` otherwise.
   */
  validateMovie: function (movie) {
    return movie.length > 0 && movie.length <= 50 && !(movie in this.movies);
  },

  /* The `toggleVisibleMyDB` method is a function that toggles the value of the `privat` property of
  the `personalMovieDB` object. */
  toggleVisibleMyDB: function () {
    this.privat = !this.privat;
    return;
  },
  renderMovieList: function () {
    const movieListElement = document.createElement("ul");
    movieListElement.id = "movieList";

    const moviesList = Object.keys(this.movies);
    moviesList.sort();
    moviesList.forEach((movie) => {
      if (Object.hasOwnProperty.call(this.movies, movie)) {
        const movieScore = this.movies[movie];
        const item = document.createElement("li");
        item.textContent = `${movie} — ${movieScore}`;
        movieListElement.append(item);
      }
    });
    return movieListElement;
  },
};

const personalMovieForm = document.querySelector("#personalMovieForm");
const filmNumberForm = personalMovieForm.querySelector("#filmNumberForm");
const addMovieForm = personalMovieForm.querySelector("#addMovieForm");

addMovieForm.addEventListener("submit", (e) => {
  e.preventDefault();

  let movie = addMovieForm.querySelector("#movie").value;
  const movieScore = addMovieForm.querySelector("#movieScore").value;
  const isFavorite = addMovieForm.querySelector("#favorite").checked;

  /* Normalize movie length */
  if (movie !== null && movie.length >= 21) {
    movie = movie.substring(0, 21 - 3) + "...";
  }

  if (personalMovieDB.validateMovie(movie)) {
    /* Add new movie to object */
    personalMovieDB.movies[movie] = movieScore;


    /* Rerender list */
    const movieList = document.querySelector("#movieList");
    const newMovieList = personalMovieDB.renderMovieList();
    movieList.replaceWith(newMovieList);

    if (isFavorite) {
      console.log("Добавляем любимый фильм");
    }
  }
});

console.log(personalMovieDB);
